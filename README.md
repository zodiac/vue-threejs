# vue-threejs

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

----

TODO:

- [] 模型底座增加文字
- [] 表单提交，获取真实用户数据
- [] 生成stl文件提供下载
- [] 美化场景和UI元素、配乐
- [] 增加转场过渡粒子特效动画
- [] 地貌外观





